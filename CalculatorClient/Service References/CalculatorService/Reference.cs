﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CalculatorClient.CalculatorService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CalculatorService.ICalculatorService")]
    public interface ICalculatorService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Addition", ReplyAction="http://tempuri.org/ICalculatorService/AdditionResponse")]
        double Addition(double leftoperand, double rightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Addition", ReplyAction="http://tempuri.org/ICalculatorService/AdditionResponse")]
        System.Threading.Tasks.Task<double> AdditionAsync(double leftoperand, double rightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Subtraction", ReplyAction="http://tempuri.org/ICalculatorService/SubtractionResponse")]
        double Subtraction(double lefoperand, double rirightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Subtraction", ReplyAction="http://tempuri.org/ICalculatorService/SubtractionResponse")]
        System.Threading.Tasks.Task<double> SubtractionAsync(double lefoperand, double rirightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Multiplication", ReplyAction="http://tempuri.org/ICalculatorService/MultiplicationResponse")]
        double Multiplication(double lefoperand, double rirightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Multiplication", ReplyAction="http://tempuri.org/ICalculatorService/MultiplicationResponse")]
        System.Threading.Tasks.Task<double> MultiplicationAsync(double lefoperand, double rirightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Division", ReplyAction="http://tempuri.org/ICalculatorService/DivisionResponse")]
        double Division(double lefoperand, double rirightoperand);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ICalculatorService/Division", ReplyAction="http://tempuri.org/ICalculatorService/DivisionResponse")]
        System.Threading.Tasks.Task<double> DivisionAsync(double lefoperand, double rirightoperand);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ICalculatorServiceChannel : CalculatorClient.CalculatorService.ICalculatorService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class CalculatorServiceClient : System.ServiceModel.ClientBase<CalculatorClient.CalculatorService.ICalculatorService>, CalculatorClient.CalculatorService.ICalculatorService {
        
        public CalculatorServiceClient() {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public CalculatorServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public double Addition(double leftoperand, double rightoperand) {
            return base.Channel.Addition(leftoperand, rightoperand);
        }
        
        public System.Threading.Tasks.Task<double> AdditionAsync(double leftoperand, double rightoperand) {
            return base.Channel.AdditionAsync(leftoperand, rightoperand);
        }
        
        public double Subtraction(double lefoperand, double rirightoperand) {
            return base.Channel.Subtraction(lefoperand, rirightoperand);
        }
        
        public System.Threading.Tasks.Task<double> SubtractionAsync(double lefoperand, double rirightoperand) {
            return base.Channel.SubtractionAsync(lefoperand, rirightoperand);
        }
        
        public double Multiplication(double lefoperand, double rirightoperand) {
            return base.Channel.Multiplication(lefoperand, rirightoperand);
        }
        
        public System.Threading.Tasks.Task<double> MultiplicationAsync(double lefoperand, double rirightoperand) {
            return base.Channel.MultiplicationAsync(lefoperand, rirightoperand);
        }
        
        public double Division(double lefoperand, double rirightoperand) {
            return base.Channel.Division(lefoperand, rirightoperand);
        }
        
        public System.Threading.Tasks.Task<double> DivisionAsync(double lefoperand, double rirightoperand) {
            return base.Channel.DivisionAsync(lefoperand, rirightoperand);
        }
    }
}
