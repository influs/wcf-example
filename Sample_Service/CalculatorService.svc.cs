﻿using System;

namespace Sample_Service
{
    public class CalculatorService : ICalculatorService
    {
        public double Addition(double leftoperand, double rightoperand)
        {
            return leftoperand + rightoperand;
        }

        public double Subtraction(double leftoperand, double rightoperand)
        {
            return leftoperand - rightoperand;
        }

        public double Multiplication(double leftoperand, double rightoperand)
        {
            return leftoperand * rightoperand;
        }

        public double Division(double leftoperand, double rightoperand)
        {
            if (Math.Abs(rightoperand) < 1e-6)
                throw new ArgumentNullException("rightoperand");
            else
                return leftoperand / rightoperand;
        }
    }
}
