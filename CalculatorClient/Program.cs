﻿using System;
using CalculatorClient.CalculatorService;

namespace CalculatorClient
{
    class Program
    {
        static void Main()
        {
            var calculator = new CalculatorServiceClient();

            Console.WriteLine("Input left operand\n");
            var leftoperand = Console.ReadLine();
            Console.WriteLine("\nInput ritht operand\n");
            var rightoperand = Console.ReadLine();
            Console.WriteLine("\nInput sign of operation\n");
            var signoperation = Console.ReadKey();

            if (signoperation.KeyChar == '+')
            {
                Console.WriteLine("\nAnswer:" + calculator.Addition(Convert.ToDouble(leftoperand), Convert.ToDouble(rightoperand)));
            }

            else if (signoperation.KeyChar == '-')
            {
                Console.WriteLine("\nAnswer:" + calculator.Subtraction(Convert.ToDouble(leftoperand), Convert.ToDouble(rightoperand)));
            }

            else if (signoperation.KeyChar == '*')
            {
                Console.WriteLine("\nAnswer:" +
                                  calculator.Multiplication(Convert.ToDouble(leftoperand),
                                      Convert.ToDouble(rightoperand)));
            }

            else if (signoperation.KeyChar == '/')
            {
                Console.WriteLine("\nAnswer:" + calculator.Division(Convert.ToDouble(leftoperand), Convert.ToDouble(rightoperand)));
            }

            else
            {
                Console.WriteLine("\nBad operation sign");
            }
            Console.ReadKey();
            Console.ReadKey();
        }
    }
}
