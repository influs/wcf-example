﻿using System.ServiceModel;

namespace Sample_Service
{
    [ServiceContract]
    public interface ICalculatorService
    {

        [OperationContract]
        double Addition(double leftoperand, double rightoperand);

        [OperationContract]
        double Subtraction(double lefoperand, double rirightoperand);

        [OperationContract]
        double Multiplication(double lefoperand, double rirightoperand);

        [OperationContract]
        double Division(double lefoperand, double rirightoperand);

    }
}
